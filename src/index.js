import {el, setChildren} from 'redom';
import validator from 'validator';
import Inputmask from "inputmask";
import valid from 'card-validator';
import MASTERCARD from './assets/images/mastercard.png';
import MIR from './assets/images/mir.png';
import VISA from './assets/images/visa.png';
import CARD from './assets/images/card.png';

/* Номера карт мир, виза, мастеркард
2200700761452324
4276161718418402
5536914151738055
*/

const cardImages = Object.freeze({
    mastercard: MASTERCARD,
    mir: MIR,
    visa: VISA,
    defaultCard: CARD,
})

const formDiv = el('div', {class: 'container py-5 col-8'});
const btn = el('button', 'Отправить', {class: "btn btn-primary", disabled: true});
setChildren(formDiv, [
    createCardNumberField('Номер карты', 'cardNumber', "9{4} 9{4} 9{4} 9{4,6}", (x) => valid.number(x).isValid),
    createInputField('Дата окончания действия карты (ММ/ГГ)', 'expireDate', "99/99", (x) => valid.expirationDate(x).isValid),
    createInputField('CVC/CVV (3 цифры на обороте карты)', 'csv', "9{3}", (x) => valid.cvv(x).isValid),
    createInputField('Email', 'mail', "email", validator.isEmail),
    btn,
]);
setChildren(window.document.body, formDiv);

function createInputField(name, ownId, mask, validatorFunc){
    const div = el('div', {class: "form-floating mb-3 col-7"});
    const input = el('input', {
        type: "text", 
        class: "form-control", 
        id: ownId, 
        required: "true",
        onblur: function(){
            if (this.value){
                validatorFunc(this.value)
                ? this.classList.remove('wrong') 
                : this.classList.add('wrong');
            }
            else{
                this.classList.remove('wrong');
            }
            checkParams()
        },
    });
    Inputmask(mask).mask(input);
    const label = el('label', name, {for: ownId});
    setChildren(div, [input, label]);
    return div;
}

function createCardNumberField(name, ownId, mask, validatorFunc){
    const div = el('div', {class: "d-flex flex-row gap-3 mb-3"});
    const image = el('img', {src: cardImages.defaultCard});
    const inputDiv = el('div', {class: "form-floating col-6"});
    const input = el('input', {
        type: "text", 
        class: "form-control", 
        id: ownId, 
        required: "true",
        onblur: function(){
            if (this.value){
                if (validatorFunc(this.value)){
                    this.classList.remove('wrong');
                    const cardType = valid.number(this.value).card.type;
                    image.src = cardImages[cardType];
                }
                else{
                    this.classList.add('wrong');
                    image.src = cardImages.defaultCard;
                }
            }
            else{
                this.classList.remove('wrong');
            }
            checkParams()
        },
    });
    Inputmask(mask).mask(input);
    const label = el('label', name, {for: ownId});
    setChildren(inputDiv, [input, label]);
    setChildren(div, [inputDiv, image])
    return div;
}

function checkParams(){
    const a = [...document.querySelectorAll('input')].map((input) => (input.value && !input.classList.contains('wrong')));
    const isValidForm = a.every((el) => el)
    isValidForm ? btn.disabled = false : btn.disabled = true;
}
